from http.server import BaseHTTPRequestHandler
def do_GET(self):
    self.send_response(200)
    self.send_header('Content-type', 'text/plain')
    self.end_headers()
    self.wfile.write(str('Hello, world!').encode())
    return
